package main

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"os"

	"gitlab.com/Intyre/pcapReader/proto"
	"gitlab.com/Intyre/pcapReader/proto/pkt"
)

func handleDataPacket(r *Reader) {
	packetCount := r.ReadInt24()
	log.Printf("packetCount: %d\n", packetCount)

	// loop until do more data
	for {
		if r.b.Len() == 0 {
			break
		}

		layerID, hasSplit := r.ReadReliability()
		log.Printf("Reliability: %s\n", layerID)
		log.Printf("hasSplit: %v\n", hasSplit)

		// size
		size := r.ReadSize()
		log.Printf("size: %v %d\n", size, r.b.Len())

		var messageNumber int32
		var sequencingIndex int32
		var orderingIndex int32
		// Each layer can have different fields

		if layerID == IDReliable ||
			layerID == IDReliableSequenced ||
			layerID == IDReliableOrdered ||
			layerID == IDReliableWithAckReceipt ||
			layerID == IDReliableOrderedWithAckReceipt {
			messageNumber = r.ReadInt24()
			log.Printf("messageNumber: %v\n", messageNumber)
		}

		if layerID == IDUnreliableSequenced ||
			layerID == IDReliableSequenced {
			sequencingIndex = r.ReadInt32()
			log.Printf("sequencingIndex: %v\n", sequencingIndex)
		}

		if layerID == IDUnreliableSequenced ||
			layerID == IDReliableSequenced ||
			layerID == IDReliableOrdered ||
			layerID == IDReliableOrderedWithAckReceipt {
			orderingIndex = r.ReadOrderIndex()
			log.Printf("orderingIndex: %v\n", orderingIndex)

		}

		var splitCount int32
		var splitID int16
		var splitIndex int32
		if hasSplit {
			splitCount = r.ReadInt32()
			splitID = r.ReadInt16()
			splitIndex = r.ReadInt32()
			fmt.Printf("SplitInfo\n\tsplitCount: %d\n\tsplitID: %d\n\tsplitIndex: %d\n", splitCount, splitID, splitIndex)
			os.Exit(1)
		}
		// log.Printf("rest: %d\n", r.b.Len())
		// fmt.Print(hex.Dump(r.b.Bytes()[:size]))
		buf := make([]byte, size)
		binary.Read(r.b, binary.BigEndian, buf)
		splitPacket(&Reader{bytes.NewBuffer(buf)})
		// if r.b.Len() > 0 {
		// 	fmt.Println("Got more data!")
		// 	// fmt.Print(hex.Dump(r.b.Bytes()))
		// 	// os.Exit(1)
		// }
		// os.Exit(1)
		// TODO: Clean this up

	}

}

func splitPacket(b *Reader) {
	pid := proto.PacketID(b.ReadByte())
	// fmt.Print(hex.Dump(r.b.Bytes()[:0]))
	fmt.Printf("pid: %s\n", pid)
	// fmt.Print(hex.Dump(r.b.Bytes()[:size-1]))

	var packet proto.Packet
	switch pid {
	case proto.IDConnectedPong:
		packet = &pkt.ConnectedPong{}
	case proto.IDConnectionRequest:
		packet = &pkt.ConnectionRequest{}
	case proto.IDConnectionRequestAccepted:
		packet = &pkt.ConnectionRequestAccepted{}
	case proto.IDConnectedPing:
		packet = &pkt.ConnectedPing{}
	case proto.IDNewIncomingConnection:
		packet = &pkt.NewIncomingConnection{}
	case proto.IDBatch:
		packet = &pkt.Batch{}
	case proto.IDSetSpawnPosition:
		packet = &pkt.SetSpawnPosition{}
	case proto.IDStartGame:
		packet = &pkt.StartGame{}
	case proto.IDSetTime:
		packet = &pkt.SetTime{}
	case proto.IDPlayerStatus:
		packet = &pkt.PlayerStatus{}
	case proto.IDSetHealth:
		packet = &pkt.SetHealth{}
	case proto.IDSetEntityData,
		proto.IDSetEntityMotion:
		// TODO: make it work
		return
	default:
		fmt.Println("Missing packet in splitPacket")
		fmt.Print(hex.Dump(b.b.Bytes()))
		os.Exit(1)
	}
	if packet != nil {
		packet.Decode(b.b)
		fmt.Printf("%#v\n", packet)
	}
	if b.b.Len() > 0 {
		fmt.Println("Got more!")
		fmt.Print(hex.Dump(b.b.Bytes()))

		// os.Exit(1)
	}
}
