package main

//go:generate stringer -type=RelID reliability.go
type RelID uint8

const (
	IDUnreliable                    RelID = 0
	IDUnreliableSequenced           RelID = 1
	IDReliable                      RelID = 2
	IDReliableOrdered               RelID = 3
	IDReliableSequenced             RelID = 4
	IDUnreliableWithAckReceipt      RelID = 5
	IDReliableWithAckReceipt        RelID = 6
	IDReliableOrderedWithAckReceipt RelID = 7
)
