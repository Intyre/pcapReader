// generated by stringer -type=RelID reliability.go; DO NOT EDIT

package main

import "fmt"

const _RelID_name = "IDUnreliableIDUnreliableSequencedIDReliableIDReliableOrderedIDReliableSequencedIDUnreliableWithAckReceiptIDReliableWithAckReceiptIDReliableOrderedWithAckReceipt"

var _RelID_index = [...]uint8{0, 12, 33, 43, 60, 79, 105, 129, 160}

func (i RelID) String() string {
	if i >= RelID(len(_RelID_index)-1) {
		return fmt.Sprintf("RelID(%d)", i)
	}
	return _RelID_name[_RelID_index[i]:_RelID_index[i+1]]
}
