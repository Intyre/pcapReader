# pcap reader for MCPE dumps

Work in progress dump reader.

If you make any changes to info.go or reliability.go make sure you run ``go generate ./...``

## Usage

    -hex
      output hexdump
    -pcap string
      path to .pcap dump

## Todo

- [ ] add all packet structures
    - [x] UnconnectedPing
    - [x] UnconnectedPong
    - [x] OpenConnectionRequest1
    - [x] OpenConnectionRequest2
    - [x] OpenConnectionResponse1
    - [x] OpenConnectionResponse2
    - [x] ConnectedPong
    - [x] ConnectionRequest
    - [x] ConnectionRequestAccepted
    - [x] ConnectedPing
    - [x] NewIncomingConnection
    - [x] Batch
    - [x] SetSpawnPotition
- [ ] encapsulation
- [ ] reliability
