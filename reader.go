package main

import (
	"bytes"
	"encoding/binary"
	"math"
)

type Reader struct {
	b *bytes.Buffer
}

func (r *Reader) ReadByte() (x byte) {
	binary.Read(r.b, binary.BigEndian, &x)
	return x
}

func (r *Reader) ReadInt16() (x int16) {
	binary.Read(r.b, binary.BigEndian, &x)
	return x
}

func (r *Reader) ReadInt32() (x int32) {
	binary.Read(r.b, binary.BigEndian, &x)
	return x
}

func (r *Reader) ReadSize() int32 {
	return int32(math.Ceil(float64(r.ReadInt16()) / 8.0))
}

func (r *Reader) ReadInt24() int32 {
	var b [3]byte
	binary.Read(r.b, binary.BigEndian, &b)
	x := int32(b[0]) | (int32(b[1]) << 8) | (int32(b[2]) << 16)
	return x
}

func (r *Reader) ReadOrderIndex() int32 {
	var b [4]byte
	binary.Read(r.b, binary.BigEndian, &b)
	x := int32(b[0]) | (int32(b[1]) << 8) | (int32(b[2]) << 16) | (int32(b[3]) << 24)
	return x
}

func (r *Reader) ReadReliability() (RelID, bool) {
	b, _ := r.b.ReadByte()
	layerID := RelID(b & byte(0xe0) >> 5)

	hasSplit := b&byte(0x10) > 0
	return layerID, hasSplit
}
