package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/Intyre/pcapReader/proto"
	"gitlab.com/Intyre/pcapReader/proto/pkt"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

var (
	pcapPath = flag.String("pcap", "", "path to .pcap dump")
	hexDump  = flag.Bool("hex", false, "output hexdump")
)

func main() {
	log.SetFlags(log.Lshortfile)

	flag.Parse()

	_, err := os.Stat(*pcapPath)
	if len(*pcapPath) == 0 || os.IsNotExist(err) {
		flag.Usage()
		os.Exit(1)
	}

	handle, err := pcap.OpenOffline(*pcapPath)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	var eth layers.Ethernet
	var ip4 layers.IPv4
	var udp layers.UDP
	var payload gopacket.Payload

	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &eth, &ip4, &udp, &payload)
	decoded := []gopacket.LayerType{}
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	for packet := range packetSource.Packets() {
		err := parser.DecodeLayers(packet.Data(), &decoded)
		if err != nil {
			// fmt.Printf("Error: %s\n", err)
			continue
		}
		fmt.Println("+" + strings.Repeat("-", 43) + "+")
		for _, layerType := range decoded {
			switch layerType {
			case layers.LayerTypeIPv4:
				fmt.Printf("| IP    | %15s | %15s |\n", ip4.SrcIP.String(), ip4.DstIP.String())
			case layers.LayerTypeUDP:
				fmt.Printf("| Ports | %15d | %15d |\n", udp.SrcPort, udp.DstPort)
			case gopacket.LayerTypePayload:
				fmt.Println("+" + strings.Repeat("-", 43) + "+")

				parsePayload(payload.Payload())
			}
		}
		fmt.Println()
	}
	log.Println("Done!")
}

func parsePayload(data []byte) {

	var packet proto.Packet

	pid := proto.PacketID(data[0])
	// PacketID 0x01 to 0x80
	if pid >= proto.IDUnconnectedPing && pid < proto.IDUserEnum {
		switch pid {
		case proto.IDUnconnectedPing:
			packet = &pkt.UnconnectedPing{}
		case proto.IDUnconnectedPong:
			packet = &pkt.UnconnectedPong{}
		case proto.IDOpenConnectionRequest1:
			packet = &pkt.OpenConnectionRequest1{}
		case proto.IDOpenConnectionResponse1:
			packet = &pkt.OpenConnectionResponse1{}
		case proto.IDOpenConnectionRequest2:
			packet = &pkt.OpenConnectionRequest2{}
		case proto.IDOpenConnectionResponse2:
			packet = &pkt.OpenConnectionResponse2{}
		default:
			log.Printf("%s\n", pid)
		}

		if packet != nil {
			packet.Decode(bytes.NewBuffer(data[1:]))
			spew.Dump(packet)
		} else {
			os.Exit(1)
		}
	} else if pid >= proto.IDUserEnum && pid <= proto.IDDataPacketF {
		handleDataPacket(&Reader{bytes.NewBuffer(data[1:])})
	} else if pid == proto.IDAck {
		// TODO: decode ack
		fmt.Println("TODO: ACK")
	} else if pid == proto.IDNack {
		// TODO: decode nack
		fmt.Println("TODO: NACK")
	} else {
		log.Printf("Missing %s\n", pid)
		os.Exit(1)
	}
}
