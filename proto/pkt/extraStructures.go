package pkt

type IPEndpoint struct {
	IPVersion byte
	IP        [4]byte
	Port      int16
}
