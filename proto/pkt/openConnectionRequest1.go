package pkt

import (
	"bytes"
	"encoding/binary"
)

type OpenConnectionRequest1 struct {
	Magic           [16]byte
	ProtocolVersion byte
	MTUPadding      []byte
}

func (p *OpenConnectionRequest1) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, &p.Magic)
	binary.Read(r, binary.BigEndian, &p.ProtocolVersion)
	p.MTUPadding = make([]byte, r.Len())
	binary.Read(r, binary.BigEndian, p.MTUPadding)
}
