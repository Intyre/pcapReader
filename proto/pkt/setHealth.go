package pkt

import (
	"bytes"
	"encoding/binary"
)

type SetHealth struct {
	Health int32
}

func (p *SetHealth) Decode(b *bytes.Buffer) {
	binary.Read(b, binary.BigEndian, p)
}
