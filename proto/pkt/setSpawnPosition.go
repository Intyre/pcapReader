package pkt

import (
	"bytes"
	"encoding/binary"
)

type SetSpawnPosition struct {
	X int32
	Y int32
	Z int32
}

func (p *SetSpawnPosition) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
