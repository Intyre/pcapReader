package pkt

import (
	"bytes"
	"encoding/binary"
)

type SetTime struct {
	Time    int
	Started byte
}

func (p *SetTime) Decode(b *bytes.Buffer) {
	binary.Read(b, binary.BigEndian, p)
}
