package pkt

import (
	"bytes"
	"encoding/binary"
)

type StartGame struct {
	Seed      int32
	Dimension byte
	Generator int32
	Gamemode  int32
	EntityID  int64
	SpawnX    int32
	SpawnY    int32
	SpawnZ    int32
	X         float32
	Y         float32
	Z         float32
	Unknown   byte
}

func (p *StartGame) Decode(b *bytes.Buffer) {
	binary.Read(b, binary.BigEndian, p)
}
