package pkt

import (
	"bytes"
	"encoding/binary"
)

type ConnectedPing struct {
	ClientTime int64
}

func (p *ConnectedPing) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
