package pkt

import (
	"bytes"
	"encoding/binary"
)

type PlayerStatus struct {
	Status int32
}

func (p *PlayerStatus) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
