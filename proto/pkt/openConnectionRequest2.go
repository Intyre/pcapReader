package pkt

import (
	"bytes"
	"encoding/binary"
)

type OpenConnectionRequest2 struct {
	Magic    [16]byte
	Endpoint IPEndpoint
	MTUSize  int16
	ClientID int64
}

func (p *OpenConnectionRequest2) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
