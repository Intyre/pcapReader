package pkt

import (
	"bytes"
	"encoding/binary"
)

type ConnectionRequestAccepted struct {
	Endpoint   IPEndpoint
	Unknown    int16
	Endpoints  [10]IPEndpoint
	ClientTime int64
	ServerTime int64
}

func (p *ConnectionRequestAccepted) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
