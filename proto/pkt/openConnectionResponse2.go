package pkt

import (
	"bytes"
	"encoding/binary"
)

type OpenConnectionResponse2 struct {
	Magic       [16]byte
	ServerID    int64
	Endpoint    IPEndpoint
	MTUSize     int16
	HasSecurity byte
}

func (p *OpenConnectionResponse2) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
