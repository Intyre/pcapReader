package pkt

import (
	"bytes"
	"encoding/binary"
)

type UnconnectedPing struct {
	ClientTime int64
	Magic      [16]byte
}

func (p *UnconnectedPing) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
