package pkt

import (
	"bytes"
	"encoding/binary"
)

type NewIncomingConnection struct {
	Endpoint   IPEndpoint
	Endpoints  [10]IPEndpoint
	ClientTime int64
	ServerTime int64
}

func (p *NewIncomingConnection) Decode(b *bytes.Buffer) {
	binary.Read(b, binary.BigEndian, p)
}
