package pkt

import (
	"bytes"
	"encoding/binary"
)

type Batch struct {
	Size    int32
	Payload []byte
}

func (p *Batch) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, &p.Size)
	p.Payload = make([]byte, p.Size)
	binary.Read(r, binary.BigEndian, p.Payload)
}
