package pkt

import (
	"bytes"
	"encoding/binary"
)

type ConnectionRequest struct {
	ClientID    int64
	Timestamp   int64
	HasSecurity byte
}

func (p *ConnectionRequest) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
