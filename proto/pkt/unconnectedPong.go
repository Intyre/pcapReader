package pkt

import (
	"bytes"
	"encoding/binary"
)

type UnconnectedPong struct {
	ClientTime int64
	ServerID   int64
	Magic      [16]byte
	ServerName string
}

func (p *UnconnectedPong) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, &p.ClientTime)
	binary.Read(r, binary.BigEndian, &p.ServerID)
	binary.Read(r, binary.BigEndian, &p.Magic)
	var len int16
	binary.Read(r, binary.BigEndian, &len)
	serverName := make([]byte, len)
	r.Read(serverName)
	p.ServerName = string(serverName)
}
