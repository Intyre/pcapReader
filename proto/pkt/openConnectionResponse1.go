package pkt

import (
	"bytes"
	"encoding/binary"
)

type OpenConnectionResponse1 struct {
	Magic       [16]byte
	ServerID    int64
	HasSecurity byte
	MTUSize     int16
}

func (p *OpenConnectionResponse1) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
