package pkt

import (
	"bytes"
	"encoding/binary"
)

type ConnectedPong struct {
	ClientTime int64
}

func (p *ConnectedPong) Decode(r *bytes.Buffer) {
	binary.Read(r, binary.BigEndian, p)
}
