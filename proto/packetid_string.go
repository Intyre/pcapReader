// generated by stringer -type=PacketID info.go; DO NOT EDIT

package proto

import "fmt"

const _PacketID_name = "IDConnectedPingIDUnconnectedPingIDConnectedPongIDOpenConnectionRequest1IDOpenConnectionResponse1IDOpenConnectionRequest2IDOpenConnectionResponse2IDConnectionRequestIDConnectionRequestAcceptedIDNewIncomingConnectionIDDisconnectionNotificationIDUnconnectedPongIDUserEnumIDDataPacket4IDDataPacket5IDDataPacket6IDDataPacket7IDDataPacket8IDDataPacket9IDDataPacketAIDDataPacketBIDDataPacketCIDDataPacketDIDDataPacketEIDDataPacketFIDPlayerStatusIDDisconnectIDBatchIDTextIDSetTimeIDStartGameIDAddPlayerIDRemovePlayerIDAddEntityIDRemoveEntityIDAddItemEntityIDTakeItemEntityIDMoveEntityIDMovePlayerIDRemoveBlockIDUpdateBlockIDNackIDExplodeIDLeveleventIDTileEventIDEntityEventIDMobEffectIDUpdateAttributesIDMobEquipmentIDMobArmorEquipmentIDInteractIDUseItemIDPlayerActionIDHurtArmorIDSetEntityDataIDSetEntityMotionIDSetEntityLinkIDSetHealthIDSetSpawnPositionIDAnimateIDRespawnIDDropItemIDContainerOpenIDContainerCloseIDContainerSetSlotIDContainerSetDataIDContainerSetContentIDCraftingDataIDCraftingEventIDAdventureSettingsIDTileEntityDataIDFullChunkDataIDAckIDPlayerList"

var _PacketID_map = map[PacketID]string{
	0:   _PacketID_name[0:15],
	1:   _PacketID_name[15:32],
	3:   _PacketID_name[32:47],
	5:   _PacketID_name[47:71],
	6:   _PacketID_name[71:96],
	7:   _PacketID_name[96:120],
	8:   _PacketID_name[120:145],
	9:   _PacketID_name[145:164],
	16:  _PacketID_name[164:191],
	19:  _PacketID_name[191:214],
	21:  _PacketID_name[214:241],
	28:  _PacketID_name[241:258],
	128: _PacketID_name[258:268],
	132: _PacketID_name[268:281],
	133: _PacketID_name[281:294],
	134: _PacketID_name[294:307],
	135: _PacketID_name[307:320],
	136: _PacketID_name[320:333],
	137: _PacketID_name[333:346],
	138: _PacketID_name[346:359],
	139: _PacketID_name[359:372],
	140: _PacketID_name[372:385],
	141: _PacketID_name[385:398],
	142: _PacketID_name[398:411],
	143: _PacketID_name[411:424],
	144: _PacketID_name[424:438],
	145: _PacketID_name[438:450],
	146: _PacketID_name[450:457],
	147: _PacketID_name[457:463],
	148: _PacketID_name[463:472],
	149: _PacketID_name[472:483],
	150: _PacketID_name[483:494],
	151: _PacketID_name[494:508],
	152: _PacketID_name[508:519],
	153: _PacketID_name[519:533],
	154: _PacketID_name[533:548],
	155: _PacketID_name[548:564],
	156: _PacketID_name[564:576],
	157: _PacketID_name[576:588],
	158: _PacketID_name[588:601],
	159: _PacketID_name[601:614],
	160: _PacketID_name[614:620],
	161: _PacketID_name[620:629],
	162: _PacketID_name[629:641],
	163: _PacketID_name[641:652],
	164: _PacketID_name[652:665],
	165: _PacketID_name[665:676],
	166: _PacketID_name[676:694],
	167: _PacketID_name[694:708],
	168: _PacketID_name[708:727],
	169: _PacketID_name[727:737],
	170: _PacketID_name[737:746],
	171: _PacketID_name[746:760],
	172: _PacketID_name[760:771],
	173: _PacketID_name[771:786],
	174: _PacketID_name[786:803],
	175: _PacketID_name[803:818],
	176: _PacketID_name[818:829],
	177: _PacketID_name[829:847],
	178: _PacketID_name[847:856],
	179: _PacketID_name[856:865],
	180: _PacketID_name[865:875],
	181: _PacketID_name[875:890],
	182: _PacketID_name[890:906],
	183: _PacketID_name[906:924],
	184: _PacketID_name[924:942],
	185: _PacketID_name[942:963],
	186: _PacketID_name[963:977],
	187: _PacketID_name[977:992],
	188: _PacketID_name[992:1011],
	189: _PacketID_name[1011:1027],
	191: _PacketID_name[1027:1042],
	192: _PacketID_name[1042:1047],
	195: _PacketID_name[1047:1059],
}

func (i PacketID) String() string {
	if str, ok := _PacketID_map[i]; ok {
		return str
	}
	return fmt.Sprintf("PacketID(%d)", i)
}
