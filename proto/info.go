package proto

//go:generate stringer -type=PacketID info.go
type PacketID byte

const (
	IDConnectedPing           PacketID = 0x00
	IDUnconnectedPing         PacketID = 0x01
	IDConnectedPong           PacketID = 0x03
	IDOpenConnectionRequest1  PacketID = 0x05
	IDOpenConnectionResponse1 PacketID = 0x06
	IDOpenConnectionRequest2  PacketID = 0x07
	IDOpenConnectionResponse2 PacketID = 0x08
	IDConnectionRequest       PacketID = 0x09

	IDConnectionRequestAccepted PacketID = 0x10
	IDNewIncomingConnection     PacketID = 0x13
	IDDisconnectionNotification PacketID = 0x15

	IDUnconnectedPong PacketID = 0x1c
	IDUserEnum        PacketID = 0x80

	IDNack PacketID = 0xa0
	IDAck  PacketID = 0xc0

	IDDataPacket4 PacketID = 0x84
	IDDataPacket5 PacketID = 0x85
	IDDataPacket6 PacketID = 0x86
	IDDataPacket7 PacketID = 0x87
	IDDataPacket8 PacketID = 0x88
	IDDataPacket9 PacketID = 0x89
	IDDataPacketA PacketID = 0x8A
	IDDataPacketB PacketID = 0x8B
	IDDataPacketC PacketID = 0x8C
	IDDataPacketD PacketID = 0x8D
	IDDataPacketE PacketID = 0x8E
	IDDataPacketF PacketID = 0x8F

	IDLogin        PacketID = 0x8F
	IDPlayerStatus PacketID = 0x90
	IDDisconnect   PacketID = 0x91
	IDBatch        PacketID = 0x92
	IDText         PacketID = 0x93
	IDSetTime      PacketID = 0x94
	IDStartGame    PacketID = 0x95
	IDAddPlayer    PacketID = 0x96
	IDRemovePlayer PacketID = 0x97
	IDAddEntity    PacketID = 0x98
	IDRemoveEntity PacketID = 0x99

	IDAddItemEntity       PacketID = 0x9A
	IDTakeItemEntity      PacketID = 0x9B
	IDMoveEntity          PacketID = 0x9C
	IDMovePlayer          PacketID = 0x9D
	IDRemoveBlock         PacketID = 0x9E
	IDUpdateBlock         PacketID = 0x9F
	IDAddPainting         PacketID = 0xA0
	IDExplode             PacketID = 0xA1
	IDLevelevent          PacketID = 0xA2
	IDTileEvent           PacketID = 0xA3
	IDEntityEvent         PacketID = 0xA4
	IDMobEffect           PacketID = 0xA5
	IDUpdateAttributes    PacketID = 0xA6
	IDMobEquipment        PacketID = 0xA7
	IDMobArmorEquipment   PacketID = 0xA8
	IDInteract            PacketID = 0xA9
	IDUseItem             PacketID = 0xAA
	IDPlayerAction        PacketID = 0xAB
	IDHurtArmor           PacketID = 0xAC
	IDSetEntityData       PacketID = 0xAD
	IDSetEntityMotion     PacketID = 0xAE
	IDSetEntityLink       PacketID = 0xAF
	IDSetHealth           PacketID = 0xB0
	IDSetSpawnPosition    PacketID = 0xB1
	IDAnimate             PacketID = 0xB2
	IDRespawn             PacketID = 0xB3
	IDDropItem            PacketID = 0xB4
	IDContainerOpen       PacketID = 0xB5
	IDContainerClose      PacketID = 0xB6
	IDContainerSetSlot    PacketID = 0xb7
	IDContainerSetData    PacketID = 0xb8
	IDContainerSetContent PacketID = 0xb9
	IDCraftingData        PacketID = 0xba
	IDCraftingEvent       PacketID = 0xbb
	IDAdventureSettings   PacketID = 0xbc
	IDTileEntityData      PacketID = 0xbd
	//PLAYERINPUT = 0xbe
	IDFullChunkData PacketID = 0xbf
	IDSetDifficulty PacketID = 0xc0
	//CHANGEDIMENSION = 0xc1
	//SETPLAYERGAMETYPE = 0xc2
	IDPlayerList PacketID = 0xc3
	//TELEMETRYEVENT = 0xc4

)
