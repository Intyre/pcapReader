package proto

import "bytes"

type Packet interface {
	Decode(*bytes.Buffer)
}
